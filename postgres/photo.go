package postgres

import (
	"database/sql"
	"fmt"
	"gitlab.com/vakoto/map-backend/model"
	"gitlab.com/vakoto/map-backend/service"
	"log"
)

var _ model.PhotoRepository = (*photoRepository)(nil)

type photoRepository struct {
	db *sql.DB
}

func NewPhotoRepository(db *sql.DB) model.PhotoRepository {
	return &photoRepository{db: db}
}

func (pr photoRepository) Save(photo model.Photo) (uint64, error) {
	q := `INSERT INTO photos (marker_id, uri) VALUES ($1, $2) RETURNING id`

	if err := pr.db.QueryRow(q, photo.MarkerId, photo.Uri).Scan(&photo.Id); err != nil {
		return 0, err
	}

	return photo.Id, nil
}

func (pr photoRepository) Update(photo model.Photo) error {
	q := `UPDATE photos SET marker_id = $1, uri = $2 WHERE id = $3`

	res, err := pr.db.Exec(q, photo.MarkerId, photo.Uri, photo.Id)
	if err != nil {
		return err
	}

	cnt, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if cnt == 0 {
		return service.ErrNotFound
	}

	return nil
}

func (pr photoRepository) RetrieveByID(id uint64) (model.Photo, error) {
	q := `SELECT marker_id, uri FROM photos WHERE id = $1`
	marker := model.Photo{Id: id}
	err := pr.db.
		QueryRow(q, id).
		Scan(&marker.MarkerId, &marker.Uri)

	if err != nil {
		empty := model.Photo{}
		if err == sql.ErrNoRows {
			return empty, service.ErrNotFound
		}
		return empty, err
	}

	return marker, nil
}

func (pr photoRepository) RemoveByID(id uint64) error {
	q := `DELETE FROM photos WHERE id = $1`
	pr.db.Exec(q, id)
	return nil
}

func (pr photoRepository) RetrieveAll(offset, limit uint64) []model.Photo {
	q := `SELECT id, marker_id, uri FROM photos ORDER BY id LIMIT $1 OFFSET $2`
	items := []model.Photo{}

	rows, err := pr.db.Query(q, limit, offset)
	if err != nil {
		log.Printf(fmt.Sprintf("Failed to retrieve photos due to %s", err))
		return []model.Photo{}
	}
	defer rows.Close()

	for rows.Next() {
		m := model.Photo{}
		if err = rows.Scan(&m.Id, &m.MarkerId, &m.Uri); err != nil {
			log.Printf(fmt.Sprintf("Failed to read retrieved photo due to %s", err))
			return []model.Photo{}
		}
		items = append(items, m)
	}

	return items
}

func (pr photoRepository) RetrieveAllByMarkerId(markerId, offset, limit uint64) []model.Photo {
	q := `SELECT id, uri FROM photos WHERE marker_id = $1 ORDER BY id LIMIT $2 OFFSET $3`
	items := []model.Photo{}

	rows, err := pr.db.Query(q, markerId, limit, offset)
	if err != nil {
		log.Printf(fmt.Sprintf("Failed to retrieve photos due to %s", err))
		return []model.Photo{}
	}
	defer rows.Close()

	for rows.Next() {
		p := model.Photo{MarkerId: markerId}
		if err = rows.Scan(&p.Id, &p.Uri); err != nil {
			log.Printf(fmt.Sprintf("Failed to read retrieved photo due to %s", err))
			return []model.Photo{}
		}
		items = append(items, p)
	}

	return items
}
