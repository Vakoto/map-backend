package postgres

import (
	"database/sql"
	"fmt"
	"gitlab.com/vakoto/map-backend/model"
	"gitlab.com/vakoto/map-backend/service"
	"log"
)

var _ model.MarkerRepository = (*markerRepository)(nil)

type markerRepository struct {
	db *sql.DB
}

func NewMarkerRepository(db *sql.DB) model.MarkerRepository {
	return &markerRepository{db: db}
}

func (mr markerRepository) Save(marker model.Marker) (uint64, error) {
	q := `INSERT INTO markers (latitude, longitude) VALUES ($1, $2) RETURNING id`

	if err := mr.db.QueryRow(q, marker.Latitude, marker.Longitude).Scan(&marker.Id); err != nil {
		return 0, err
	}

	return marker.Id, nil
}

func (mr markerRepository) Update(marker model.Marker) error {
	q := `UPDATE markers SET latitude = $1, longitude = $2 WHERE id = $3`

	res, err := mr.db.Exec(q, marker.Latitude, marker.Longitude, marker.Id)
	if err != nil {
		return err
	}

	cnt, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if cnt == 0 {
		return service.ErrNotFound
	}

	return nil
}

func (mr markerRepository) RetrieveByID(id uint64) (model.Marker, error) {
	q := `SELECT latitude, longitude FROM markers WHERE id = $1`
	marker := model.Marker{Id: id}
	err := mr.db.
		QueryRow(q, id).
		Scan(&marker.Latitude, &marker.Longitude)

	if err != nil {
		empty := model.Marker{}
		if err == sql.ErrNoRows {
			return empty, service.ErrNotFound
		}
		return empty, err
	}

	return marker, nil
}

func (mr markerRepository) RemoveByID(id uint64) error {
	q := `DELETE FROM markers WHERE id = $1`
	mr.db.Exec(q, id)
	return nil
}

func (mr markerRepository) RetrieveAll(offset, limit uint64) []model.Marker {
	q := `SELECT id, latitude, longitude FROM markers ORDER BY id LIMIT $1 OFFSET $2`
	items:=  []model.Marker{}

	rows, err := mr.db.Query(q, limit, offset)
	if err != nil {
		log.Printf(fmt.Sprintf("Failed to retrieve markers due to %s", err))
		return []model.Marker{}
	}
	defer rows.Close()

	for rows.Next() {
		m := model.Marker{}
		if err = rows.Scan(&m.Id, &m.Latitude, &m.Longitude); err != nil {
			log.Printf(fmt.Sprintf("Failed to read retrieved marker due to %s", err))
			return []model.Marker{}
		}
		items = append(items, m)
	}

	return items
}
