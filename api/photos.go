package api

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/vakoto/map-backend/model"
	"net/http"
	"net/url"
)

func getPhotoEndpoint(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := toUInt64(params["id"])
	if err != nil {
		encodeError(err, w)
		return
	}

	photo, err := svc.GetPhoto(id)
	if err != nil {
		encodeError(err, w)
		return
	}

	jsonData, err := json.Marshal(photo)
	if err != nil {
		encodeError(err, w)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}

func createPhotoEndpoint(w http.ResponseWriter, r *http.Request) {
	var photo model.Photo
	if err := json.NewDecoder(r.Body).Decode(&photo); err != nil {
		encodeError(err, w)
		return
	}

	photo, err := svc.AddPhoto(photo)
	if err != nil {
		encodeError(err, w)
		return
	}

	jsonData, err := json.Marshal(photo)
	if err != nil {
		encodeError(err, w)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}

func deletePhotoEndpoint(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := toUInt64(params["id"])
	if err != nil {
		encodeError(err, w)
		return
	}

	if err = svc.RemovePhoto(id); err != nil {
		encodeError(err, w)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func updatePhotoEndpoint(w http.ResponseWriter, r *http.Request) {
	var photo model.Photo
	if err := json.NewDecoder(r.Body).Decode(&photo); err != nil {
		encodeError(err, w)
		return
	}

	if err := svc.UpdatePhoto(photo); err != nil {
		encodeError(err, w)
		return
	}

	jsonData, err := json.Marshal(photo)
	if err != nil {
		encodeError(err, w)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}

func getPhotosEndpoint(w http.ResponseWriter, r *http.Request) {
	q, err := url.ParseQuery(r.URL.RawQuery)
	if err != nil {
		encodeError(errInvalidQueryParams, w)
		return
	}

	offset := uint64(0)
	limit := uint64(100)

	off, lmt := q["offset"], q["limit"]

	if len(off) > 1 || len(lmt) > 1 {
		encodeError(errInvalidQueryParams, w)
		return
	}

	if len(off) == 1 {
		offset, err = toUInt64(off[0])
		if err != nil {
			encodeError(errInvalidQueryParams, w)
			return
		}
	}

	if len(lmt) == 1 {
		limit, err = toUInt64(lmt[0])
		if err != nil {
			encodeError(errInvalidQueryParams, w)
			return
		}
	}

	photos, err := svc.GetPhotos(offset, limit)
	if err != nil {
		encodeError(err, w)
		return
	}

	jsonData, err := json.Marshal(photos)
	if err != nil {
		encodeError(err, w)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}

func getPhotosByMarkerIdEndpoint(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	markerId, err := toUInt64(params["marker_id"])
	if err != nil {
		encodeError(err, w)
		return
	}

	q, err := url.ParseQuery(r.URL.RawQuery)
	if err != nil {
		encodeError(errInvalidQueryParams, w)
		return
	}

	offset := uint64(0)
	limit := uint64(100)

	off, lmt := q["offset"], q["limit"]

	if len(off) > 1 || len(lmt) > 1 {
		encodeError(errInvalidQueryParams, w)
		return
	}

	if len(off) == 1 {
		offset, err = toUInt64(off[0])
		if err != nil {
			encodeError(errInvalidQueryParams, w)
			return
		}
	}

	if len(lmt) == 1 {
		limit, err = toUInt64(lmt[0])
		if err != nil {
			encodeError(errInvalidQueryParams, w)
			return
		}
	}

	photos, err := svc.GetPhotosByMarkerId(markerId, offset, limit)
	if err != nil {
		encodeError(err, w)
		return
	}

	jsonData, err := json.Marshal(photos)
	if err != nil {
		encodeError(err, w)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}
