package service

import (
	"encoding/json"
	"fmt"
	"gitlab.com/vakoto/map-backend/model"
	"log"
	"time"
)

var _ Service = (*loggingMiddleware)(nil)

type loggingMiddleware struct {
	svc Service
}

func LoggingMiddleware(svc Service) Service {
	return &loggingMiddleware{svc}
}

func toJson(v interface{}) string {
	// ignore possible errors
	json, _ := json.Marshal(v)
	return string(json)
}

func (lm *loggingMiddleware) AddMarker(marker model.Marker) (added model.Marker, err error) {
	defer func(begin time.Time) {
		message := fmt.Sprintf("Method add_marker for marker %s took %s to complete", toJson(added), time.Since(begin))
		if err != nil {
			log.Printf(fmt.Sprintf("%s with error: %s.", message, err))
			return
		}
		log.Printf(fmt.Sprintf("%s without errors.", message))
	}(time.Now())

	return lm.svc.AddMarker(marker)
}

func (lm *loggingMiddleware) UpdateMarker(marker model.Marker) (err error) {
	defer func(begin time.Time) {
		message := fmt.Sprintf("Method update_marker for marker %s took %s to complete", toJson(marker), time.Since(begin))
		if err != nil {
			log.Printf(fmt.Sprintf("%s with error: %s.", message, err))
			return
		}
		log.Printf(fmt.Sprintf("%s without errors.", message))
	}(time.Now())

	return lm.svc.UpdateMarker(marker)
}

func (lm *loggingMiddleware) GetMarker(id uint64) (marker model.Marker, err error) {
	defer func(begin time.Time) {
		message := fmt.Sprintf("Method get_marker for id %d took %s to complete", id, time.Since(begin))
		if err != nil {
			log.Printf(fmt.Sprintf("%s with error: %s.", message, err))
			return
		}
		log.Printf(fmt.Sprintf("%s without errors.", message))
	}(time.Now())

	return lm.svc.GetMarker(id)
}

func (lm *loggingMiddleware) RemoveMarker(id uint64) (err error) {
	defer func(begin time.Time) {
		message := fmt.Sprintf("Method remove_marker for marker with id %d took %s to complete", id, time.Since(begin))
		if err != nil {
			log.Printf(fmt.Sprintf("%s with error: %s.", message, err))
			return
		}
		log.Printf(fmt.Sprintf("%s without errors.", message))
	}(time.Now())

	return lm.svc.RemoveMarker(id)
}

func (lm *loggingMiddleware) GetMarkers(offset, limit uint64) (_ []model.Marker, err error) {
	defer func(begin time.Time) {
		message := fmt.Sprintf("Method get_markers took %s to complete", time.Since(begin))
		if err != nil {
			log.Printf(fmt.Sprintf("%s with error: %s.", message, err))
			return
		}
		log.Printf(fmt.Sprintf("%s without errors.", message))
	}(time.Now())

	return lm.svc.GetMarkers(offset, limit)
}

func (lm *loggingMiddleware) AddPhoto(photo model.Photo) (added model.Photo, err error) {
	defer func(begin time.Time) {
		message := fmt.Sprintf("Method add_photo for photo %s took %s to complete", toJson(added), time.Since(begin))
		if err != nil {
			log.Printf(fmt.Sprintf("%s with error: %s.", message, err))
			return
		}
		log.Printf(fmt.Sprintf("%s without errors.", message))
	}(time.Now())

	return lm.svc.AddPhoto(photo)
}

func (lm *loggingMiddleware) UpdatePhoto(photo model.Photo) (err error) {
	defer func(begin time.Time) {
		message := fmt.Sprintf("Method update_photo for photo %s took %s to complete", toJson(photo), time.Since(begin))
		if err != nil {
			log.Printf(fmt.Sprintf("%s with error: %s.", message, err))
			return
		}
		log.Printf(fmt.Sprintf("%s without errors.", message))
	}(time.Now())

	return lm.svc.UpdatePhoto(photo)
}

func (lm *loggingMiddleware) GetPhoto(id uint64) (photo model.Photo, err error) {
	defer func(begin time.Time) {
		message := fmt.Sprintf("Method get_photo for id %d took %s to complete", id, time.Since(begin))
		if err != nil {
			log.Printf(fmt.Sprintf("%s with error: %s.", message, err))
			return
		}
		log.Printf(fmt.Sprintf("%s without errors.", message))
	}(time.Now())

	return lm.svc.GetPhoto(id)
}

func (lm *loggingMiddleware) RemovePhoto(id uint64) (err error) {
	defer func(begin time.Time) {
		message := fmt.Sprintf("Method remove_photo for photo with id %d took %s to complete", id, time.Since(begin))
		if err != nil {
			log.Printf(fmt.Sprintf("%s with error: %s.", message, err))
			return
		}
		log.Printf(fmt.Sprintf("%s without errors.", message))
	}(time.Now())

	return lm.svc.RemovePhoto(id)
}

func (lm *loggingMiddleware) GetPhotos(offset, limit uint64) (_ []model.Photo, err error) {
	defer func(begin time.Time) {
		message := fmt.Sprintf("Method get_photos took %s to complete", time.Since(begin))
		if err != nil {
			log.Printf(fmt.Sprintf("%s with error: %s.", message, err))
			return
		}
		log.Printf(fmt.Sprintf("%s without errors.", message))
	}(time.Now())

	return lm.svc.GetPhotos(offset, limit)
}

func (lm *loggingMiddleware) GetPhotosByMarkerId(markerId, offset, limit uint64) (_ []model.Photo, err error) {
	defer func(begin time.Time) {
		message := fmt.Sprintf("Method get_photos_by_marker_id for markerId = %d took %s to complete", markerId, time.Since(begin))
		if err != nil {
			log.Printf(fmt.Sprintf("%s with error: %s.", message, err))
			return
		}
		log.Printf(fmt.Sprintf("%s without errors.", message))
	}(time.Now())

	return lm.svc.GetPhotosByMarkerId(markerId, offset, limit)
}
