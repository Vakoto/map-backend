package api

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"gitlab.com/vakoto/map-backend/service"
	"io"
	"net/http"
	"strconv"
)

var (
	svc                   service.Service
	errInvalidQueryParams = errors.New("invalid query params")
)

func toUInt64(v string) (uint64, error) {
	base := 10
	bitSize := 64

	id, err := strconv.ParseUint(v, base, bitSize)
	if err != nil {
		return 0, errInvalidQueryParams
	}

	return id, nil
}

func MakeHandler(_svc service.Service) *mux.Router {
	svc = _svc
	router := mux.NewRouter()

	router.HandleFunc("/markers", getMarkersEndpoint).Methods("GET")
	router.HandleFunc("/markers/{id}", getMarkerEndpoint).Methods("GET")
	router.HandleFunc("/markers", createMarkerEndpoint).Methods("POST")
	router.HandleFunc("/markers/{id}", deleteMarkerEndpoint).Methods("DELETE")
	router.HandleFunc("/markers", updateMarkerEndpoint).Methods("PUT")

	router.HandleFunc("/markers/{marker_id}/photos", getPhotosByMarkerIdEndpoint).Methods("GET")
	router.HandleFunc("/photos", getPhotosEndpoint).Methods("GET")
	router.HandleFunc("/photos/{id}", getPhotoEndpoint).Methods("GET")
	router.HandleFunc("/photos", createPhotoEndpoint).Methods("POST")
	router.HandleFunc("/photos/{id}", deletePhotoEndpoint).Methods("DELETE")
	router.HandleFunc("/photos", updatePhotoEndpoint).Methods("PUT")

	return router
}

func encodeError(err error, w http.ResponseWriter) {
	switch err {
	case service.ErrNotFound:
		w.WriteHeader(http.StatusNotFound)
	case errInvalidQueryParams:
		w.WriteHeader(http.StatusBadRequest)
	case io.ErrUnexpectedEOF:
		w.WriteHeader(http.StatusBadRequest)
	case io.EOF:
		w.WriteHeader(http.StatusBadRequest)
	default:
		switch err.(type) {
		case *json.SyntaxError:
			w.WriteHeader(http.StatusBadRequest)
		case *json.UnmarshalTypeError:
			w.WriteHeader(http.StatusBadRequest)
		default:
			w.WriteHeader(http.StatusInternalServerError)
		}
	}
}
