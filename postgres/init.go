package postgres

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	"github.com/rubenv/sql-migrate"
)

type Config struct {
	DriverName string
	Host       string
	Port       string
	User       string
	Password   string
	DbName     string
}

func Connect(config Config) (*sql.DB, error) {
	url := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		config.Host, config.Port, config.User, config.DbName, config.Password)

	db, err := sql.Open("postgres", url)
	if err != nil {
		return nil, err
	}

	if err := migrateDB(db); err != nil {
		return nil, err
	}

	return db, nil
}

func migrateDB(db *sql.DB) error {
	migrations := &migrate.MemoryMigrationSource{
		Migrations: []*migrate.Migration{
			{
				Id: "map_backend",
				Up: []string{
					`CREATE TABLE IF NOT EXISTS markers (
						id			BIGSERIAL,
						latitude	DOUBLE PRECISION,
						longitude	DOUBLE PRECISION,
						PRIMARY KEY (id)
					)`,
					`CREATE TABLE IF NOT EXISTS photos (
						id			BIGSERIAL,
						marker_id	BIGINT,
						uri			TEXT,
						FOREIGN KEY (marker_id) REFERENCES markers (id) ON DELETE CASCADE ON UPDATE CASCADE,
						PRIMARY KEY (id)
					)`,
				},

				Down: []string{
					"DROP TABLE photos",
					"DROP TABLE markers",
				},
			},
		},
	}

	_, err := migrate.Exec(db, "postgres", migrations, migrate.Down)
	if err == nil {
		_, err = migrate.Exec(db, "postgres", migrations, migrate.Up)
	}
	return err
}
