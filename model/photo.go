package model

type Photo struct {
	Id       uint64 `json:"id"`
	MarkerId uint64 `json:"marker_id"`
	Uri      string `json:"uri"`
}

type PhotoRepository interface {
	Save(Photo) (uint64, error)
	Update(Photo) error
	RetrieveByID(uint64) (Photo, error)
	RemoveByID(uint64) error
	RetrieveAll(uint64, uint64) []Photo
	RetrieveAllByMarkerId(uint64, uint64, uint64) []Photo
}
