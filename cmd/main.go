package main

import (
	"gitlab.com/vakoto/map-backend/api"
	"gitlab.com/vakoto/map-backend/postgres"
	"gitlab.com/vakoto/map-backend/service"
	"log"
	"net/http"
)

const address = ":5005"

func main() {
	config := postgres.Config{
		DriverName: "postgres",
		Host:       "localhost",
		Port:       "5432",
		User:       "postgres",
		Password:   "postgres",
		DbName:     "map_backend",
	}

	db, err := postgres.Connect(config)
	if err != nil {
		log.Fatalf("Failed to connect to postgres: %s", err)
	}
	defer db.Close()

	markersRepo := postgres.NewMarkerRepository(db)
	photosRepo := postgres.NewPhotoRepository(db)

	svc := service.New(markersRepo, photosRepo)
	svc = service.LoggingMiddleware(svc)

	log.Printf("Map service started on %s.", address)
	log.Fatal(http.ListenAndServe(address, api.MakeHandler(svc)))
}
