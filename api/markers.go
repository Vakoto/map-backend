package api

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/vakoto/map-backend/model"
	"net/http"
	"net/url"
)

func getMarkerEndpoint(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := toUInt64(params["id"])
	if err != nil {
		encodeError(err, w)
		return
	}

	marker, err := svc.GetMarker(id)
	if err != nil {
		encodeError(err, w)
		return
	}

	jsonData, err := json.Marshal(marker)
	if err != nil {
		encodeError(err, w)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}

func createMarkerEndpoint(w http.ResponseWriter, r *http.Request) {
	var marker model.Marker
	if err := json.NewDecoder(r.Body).Decode(&marker); err != nil {
		encodeError(err, w)
		return
	}

	marker, err := svc.AddMarker(marker)
	if err != nil {
		encodeError(err, w)
		return
	}

	jsonData, err := json.Marshal(marker)
	if err != nil {
		encodeError(err, w)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}

func deleteMarkerEndpoint(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := toUInt64(params["id"])
	if err != nil {
		encodeError(err, w)
		return
	}

	if err = svc.RemoveMarker(id); err != nil {
		encodeError(err, w)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func updateMarkerEndpoint(w http.ResponseWriter, r *http.Request) {
	var marker model.Marker
	if err := json.NewDecoder(r.Body).Decode(&marker); err != nil {
		encodeError(err, w)
		return
	}

	if err := svc.UpdateMarker(marker); err != nil {
		encodeError(err, w)
		return
	}

	jsonData, err := json.Marshal(marker)
	if err != nil {
		encodeError(err, w)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}

func getMarkersEndpoint(w http.ResponseWriter, r *http.Request) {
	q, err := url.ParseQuery(r.URL.RawQuery)
	if err != nil {
		encodeError(errInvalidQueryParams, w)
		return
	}

	offset := uint64(0)
	limit := uint64(100)

	off, lmt := q["offset"], q["limit"]

	if len(off) > 1 || len(lmt) > 1 {
		encodeError(errInvalidQueryParams, w)
		return
	}

	if len(off) == 1 {
		offset, err = toUInt64(off[0])
		if err != nil {
			encodeError(errInvalidQueryParams, w)
			return
		}
	}

	if len(lmt) == 1 {
		limit, err = toUInt64(lmt[0])
		if err != nil {
			encodeError(errInvalidQueryParams, w)
			return
		}
	}

	markers, err := svc.GetMarkers(offset, limit)
	if err != nil {
		encodeError(err, w)
		return
	}

	jsonData, err := json.Marshal(markers)
	if err != nil {
		encodeError(err, w)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}
