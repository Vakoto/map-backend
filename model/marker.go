package model

type Marker struct {
	Id        uint64  `json:"id"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type MarkerRepository interface {
	Save(Marker) (uint64, error)
	Update(Marker) error
	RetrieveByID(uint64) (Marker, error)
	RemoveByID(uint64) error
	RetrieveAll(uint64, uint64) []Marker
}
