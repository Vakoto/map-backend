package service

import (
	"errors"
	"gitlab.com/vakoto/map-backend/model"
)

var ErrNotFound = errors.New("non-existent entity")

type Service interface {
	AddMarker(model.Marker) (model.Marker, error)
	UpdateMarker(model.Marker) error
	GetMarker(uint64) (model.Marker, error)
	RemoveMarker(uint64) error
	GetMarkers(uint64, uint64) ([]model.Marker, error)

	AddPhoto(model.Photo) (model.Photo, error)
	UpdatePhoto(model.Photo) error
	GetPhoto(uint64) (model.Photo, error)
	RemovePhoto(uint64) error
	GetPhotos(uint64, uint64) ([]model.Photo, error)
	GetPhotosByMarkerId(uint64, uint64, uint64) ([]model.Photo, error)
}

var _ Service = (*service)(nil)

type service struct {
	markers model.MarkerRepository
	photos  model.PhotoRepository
}

func New(markers model.MarkerRepository, photos model.PhotoRepository) Service {
	return &service{
		markers: markers,
		photos:  photos,
	}
}

func (s *service) AddMarker(marker model.Marker) (model.Marker, error) {
	id, err := s.markers.Save(marker)
	if err != nil {
		return model.Marker{}, err
	}

	marker.Id = id
	return marker, nil
}

func (s *service) UpdateMarker(marker model.Marker) error {
	return s.markers.Update(marker)
}

func (s *service) GetMarker(id uint64) (model.Marker, error) {
	return s.markers.RetrieveByID(id)
}

func (s *service) RemoveMarker(id uint64) error {
	return s.markers.RemoveByID(id)
}

func (s *service) GetMarkers(offset, limit uint64) ([]model.Marker, error) {
	return s.markers.RetrieveAll(offset, limit), nil
}

func (s *service) AddPhoto(photo model.Photo) (model.Photo, error) {
	id, err := s.photos.Save(photo)
	if err != nil {
		return model.Photo{}, err
	}

	photo.Id = id
	return photo, nil
}

func (s *service) UpdatePhoto(photo model.Photo) error {
	return s.photos.Update(photo)
}

func (s *service) GetPhoto(id uint64) (model.Photo, error) {
	return s.photos.RetrieveByID(id)
}

func (s *service) RemovePhoto(id uint64) error {
	return s.photos.RemoveByID(id)
}

func (s *service) GetPhotos(offset, limit uint64) ([]model.Photo, error) {
	return s.photos.RetrieveAll(offset, limit), nil
}

func (s *service) GetPhotosByMarkerId(markerId, offset, limit uint64) ([]model.Photo, error) {
	return s.photos.RetrieveAllByMarkerId(markerId, offset, limit), nil
}
